import React, { useState } from "react";
import {
	View,
	TextInput,
	StyleSheet,
	Button,
	TouchableWithoutFeedback,
	Keyboard,
} from "react-native";

const GoalInput = ({ navigation, onAddGoal, isVisible }) => {
	const [enteredGoal, setEnteredGoal] = useState("");

	const goalInputHandler = enteredText => {
		setEnteredGoal(enteredText);
	};

	const addGoalHandler = () => {
		navigation.goBack();
		onAddGoal(enteredGoal);
		setEnteredGoal("");
	};

	return (
		<TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
			<View style={styles.inputContainer}>
				<TextInput
					placeholder="Your goal"
					style={styles.input}
					onChangeText={goalInputHandler}
					value={enteredGoal}
				/>
				<View style={styles.buttonsContainer}>
					<View style={styles.button}>
						<Button
							title="CANCEL"
							color="red"
							onPress={() => navigation.goBack()}
						/>
					</View>
					<View style={styles.button}>
						<Button title="ADD" onPress={addGoalHandler} />
					</View>
				</View>
			</View>
		</TouchableWithoutFeedback>
	);
};

const styles = StyleSheet.create({
	inputContainer: {
		flexGrow: 1,
		justifyContent: "center",
		alignItems: "center",
		paddingBottom: "30%",
	},
	input: {
		borderColor: "black",
		borderWidth: 1,
		padding: 10,
		width: "80%",
		marginBottom: 10,
	},
	buttonsContainer: {
		flexDirection: "row",
		justifyContent: "space-between",
		width: "60%",
	},
	button: {
		width: "40%",
	},
});

export default GoalInput;
