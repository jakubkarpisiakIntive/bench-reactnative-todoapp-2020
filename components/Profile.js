import React from "react";
import { View, StyleSheet, Image, Button } from "react-native";
import * as ImagePicker from "expo-image-picker";
import * as Sharing from "expo-sharing";
import { useActionSheet } from "@expo/react-native-action-sheet";

const Profile = () => {
	const [selectedImage, setSelectedImage] = React.useState({
		uri: "https://i.imgur.com/mCHMpLT.png",
		local: false,
	});

	const { showActionSheetWithOptions } = useActionSheet();

	const openActionSheet = () => {
		const title = "Pick a photo";
		const options = ["From gallery", "Use camera", "Cancel"];
		const cancelButtonIndex = 2;

		showActionSheetWithOptions(
			{
				options,
				title,
				cancelButtonIndex,
			},
			buttonIndex => {
				switch (buttonIndex) {
					case 0:
						openImagePickerAsync();
						break;
					case 1:
						openCameraPickerAsync();
						break;
					default:
						break;
				}
			},
		);
	};

	let openImagePickerAsync = async () => {
		let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();

		if (permissionResult.granted === false) {
			alert("Permission to access camera roll is required!");
			return;
		}

		let pickerResult = await ImagePicker.launchImageLibraryAsync({
			allowsEditing: true,
		});
		if (pickerResult.cancelled === true) {
			return;
		}

		setSelectedImage({ uri: pickerResult.uri, local: true });
	};

	const openCameraPickerAsync = async () => {
		let permissionResult = await ImagePicker.requestCameraRollPermissionsAsync();
		let permissionResult2 = await ImagePicker.requestCameraPermissionsAsync();

		if (!permissionResult.granted || !permissionResult2.granted) {
			alert("Permission to access camera and camera roll is required!");
			return;
		}

		let pickerResult = await ImagePicker.launchCameraAsync({
			allowsEditing: true,
		});

		if (!pickerResult.cancelled) {
			setSelectedImage({ uri: pickerResult.uri, local: true });
		}
	};

	let openShareDialogAsync = async () => {
		if (!(await Sharing.isAvailableAsync())) {
			alert(`Uh oh, sharing isn't available on your platform`);
			return;
		}

		Sharing.shareAsync(selectedImage.uri);
	};

	return (
		<View style={styles.container}>
			<View>
				<Image source={selectedImage} style={styles.logo} resizeMode="cover" />
			</View>
			<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
				<View>
					<Button onPress={openActionSheet} title="Pick a photo"></Button>
				</View>
				{selectedImage.local && (
					<View style={{ marginLeft: 10 }}>
						<Button
							onPress={openShareDialogAsync}
							title="Share the photo"
						></Button>
					</View>
				)}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
	logo: {
		width: 200,
		height: 200,
		marginBottom: 20,
		borderRadius: 200 / 2,
	},
	instructions: {
		color: "#888",
		fontSize: 18,
		marginHorizontal: 15,
		marginBottom: 10,
	},
	button: {
		backgroundColor: "blue",
		padding: 20,
		borderRadius: 5,
	},
	buttonText: {
		fontSize: 16,
	},
});

export default Profile;
