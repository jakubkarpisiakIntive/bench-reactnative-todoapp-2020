import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList, Button, Alert } from "react-native";
import GoalItem from "./GoalItem";
import GoalInput from "./GoalInput";
import { createStackNavigator } from "@react-navigation/stack";

export default function Goals({ navigation }) {
	const GoalStack = createStackNavigator();

	const [courseGoals, setCourseGoals] = useState([]);
	const [isAddMode, setIsAddMode] = useState(false);

	useEffect(() => {
		if (courseGoals.length > 0) {
			navigation.setOptions({ title: `My Goals (${courseGoals.length})` });
		} else {
			navigation.setOptions({ title: `My Goals` });
		}
	}, [courseGoals.length]);

	const addGoalHandler = goalTitle => {
		if (goalTitle.length === 0) {
			Alert.alert("Ooops!", "Your goal must not be empty", [
				{
					text: "Understood",
				},
			]);
			return;
		}
		setCourseGoals(currentGoals => [
			...currentGoals,
			{ id: Math.random().toString(), value: goalTitle },
		]);
		setIsAddMode(false);
	};

	const removeGoalHandler = goalId => {
		setCourseGoals(currentGoals =>
			currentGoals.filter(goal => goal.id !== goalId),
		);
	};

	return (
		<GoalStack.Navigator headerMode="none">
			<GoalStack.Screen name="Goals">
				{navigatorProps => (
					<View style={styles.screen}>
						<Button
							title="Add new goal"
							onPress={() => navigation.navigate("Modal")}
						/>

						<FlatList
							data={courseGoals}
							renderItem={itemData => (
								<GoalItem
									id={itemData.item.id}
									title={itemData.item.value}
									onDelete={removeGoalHandler}
								/>
							)}
						/>
					</View>
				)}
			</GoalStack.Screen>
			<GoalStack.Screen name="Modal">
				{navigatorProps => (
					<GoalInput
						navigation={navigatorProps.navigation}
						onAddGoal={addGoalHandler}
						isVisible={isAddMode}
					/>
				)}
			</GoalStack.Screen>
		</GoalStack.Navigator>
	);
}

const styles = StyleSheet.create({
	screen: {
		padding: 50,
	},
});
