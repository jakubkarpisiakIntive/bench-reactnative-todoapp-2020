import React, { useContext } from "react";
import { View, StyleSheet, Switch, Text } from "react-native";
import { useTheme } from "@react-navigation/native";
import { ColorContext } from "../App";

const Settings = () => {
	const { colors } = useTheme();

	const { colorTheme, setColorTheme } = useContext(ColorContext);

	return (
		<View style={styles.container}>
			<View style={styles.row}>
				<Text style={{ color: colors.text }}>Use dark mode</Text>
				<Switch
					value={colorTheme === "dark" ? true : false}
					onValueChange={v => {
						if (!!v) {
							setColorTheme("dark");
						} else {
							setColorTheme("light");
						}
					}}
				/>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 50,
	},
	row: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
	},
});

export default Settings;
