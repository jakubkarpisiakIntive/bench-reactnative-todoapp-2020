import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

const GoalItem = ({ id, title, onDelete }) => {
	return (
		<View style={styles.courseListItem}>
			<Text style={{ fontFamily: "roboto-bold" }}>{title}</Text>
			<MaterialIcons
				name="delete"
				size={18}
				color="#aaa"
				onPress={() => onDelete(id)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	courseListItem: {
		padding: 10,
		backgroundColor: "#eee",
		borderColor: "#bbb",
		borderStyle: "dotted",
		borderRadius: 10,
		borderWidth: 1,
		marginVertical: 10,
		flexDirection: "row",
		justifyContent: "space-between",
		fontFamily: "roboto-bold",
	},
});

export default GoalItem;
