import * as React from "react";
import { StyleSheet, SafeAreaView, StatusBar, Platform } from "react-native";
import Profile from "./components/Profile";
import Goals from "./components/Goals";
import Settings from "./components/Settings";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import * as Font from "expo-font";
import { AppLoading, Linking } from "expo";
import {
	NavigationContainer,
	useLinking,
	DefaultTheme,
	DarkTheme,
} from "@react-navigation/native";
import { AppearanceProvider, useColorScheme } from "react-native-appearance";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

const loadFonts = () => {
	return Font.loadAsync({
		"roboto-bold": require("./assets/fonts/Roboto-Bold.ttf"),
		"roboto-regular": require("./assets/fonts/Roboto-Regular.ttf"),
	});
};

const baseHeaderStyle = {
	headerStyle: {
		backgroundColor: "#e91e63",
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		fontWeight: "bold",
	},
};

const useDeepLink = ref => {
	const prefix = Linking.makeUrl("/");

	const { getInitialState } = useLinking(ref, {
		prefixes: [prefix],
	});

	const [isReady, setIsReady] = React.useState(false);
	const [initialState, setInitialState] = React.useState();

	React.useEffect(() => {
		getInitialState()
			.catch(() => {})
			.then(state => {
				if (state !== undefined) {
					setInitialState(state);
				}

				setIsReady(true);
			});
	}, [getInitialState]);

	return [isReady, initialState];
};

// require cycle - move it to separate file?
export const ColorContext = React.createContext({ colorTheme: "light" });

export default function App() {
	const ref = React.useRef();

	const [isReady, initialState] = useDeepLink(ref);

	const [fontsLoaded, setFontsLoaded] = React.useState(false);

	const colorScheme = useColorScheme();
	const [colorTheme, setColorTheme] = React.useState(colorScheme);

	const Tab = createMaterialTopTabNavigator();

	if (fontsLoaded && isReady) {
		return (
			<SafeAreaView style={styles.app}>
				<AppearanceProvider>
					<ActionSheetProvider>
						<ColorContext.Provider value={{ colorTheme, setColorTheme }}>
							<NavigationContainer
								theme={colorTheme === "dark" ? DarkTheme : DefaultTheme}
								initialState={initialState}
								ref={ref}
							>
								<Tab.Navigator screenOptions={{ ...baseHeaderStyle }}>
									<Tab.Screen
										name="Goals"
										component={Goals}
										options={{ title: "My Goals" }}
									/>
									<Tab.Screen name="Profile" component={Profile} />
									<Tab.Screen name="Settings" component={Settings} />
								</Tab.Navigator>
							</NavigationContainer>
						</ColorContext.Provider>
					</ActionSheetProvider>
				</AppearanceProvider>
			</SafeAreaView>
		);
	} else {
		return (
			<AppLoading
				startAsync={loadFonts}
				onFinish={() => setFontsLoaded(true)}
			/>
		);
	}
}

const styles = StyleSheet.create({
	app: {
		flex: 1,
		paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
	},
	tabbar: {
		backgroundColor: "#e91e63",
	},
	indicator: {
		backgroundColor: "#ffeb3b",
	},
});
